package ec.unl.cis.pestscanner.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ec.unl.cis.pestscanner.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MisCapturasFragment extends Fragment {


    public MisCapturasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mis_capturas, container, false);
    }

}
