package ec.unl.cis.pestscanner.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import ec.unl.cis.pestscanner.R;
import ec.unl.cis.pestscanner.view.fragment.GuiaFragment;
import ec.unl.cis.pestscanner.view.fragment.MisCapturasFragment;
import ec.unl.cis.pestscanner.view.fragment.ScannerFragment;

public class ContainerActivity extends AppCompatActivity {
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_scanear:
                    Context context = getApplicationContext();
                    CharSequence text = "Aca se podra scanear las plantas";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                   ScannerFragment  scannerFragment = new ScannerFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,scannerFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .addToBackStack(null).commit();

                    return true;
                case R.id.navigation_Scaneadas:
                    Context context1 = getApplicationContext();
                    CharSequence text1 = "Aca se vera una galeria de la capturas";
                    int duration1 = Toast.LENGTH_SHORT;
                    Toast toast1 = Toast.makeText(context1, text1, duration1);
                    toast1.show();
                    MisCapturasFragment misCapturasFragment = new MisCapturasFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,misCapturasFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .addToBackStack(null).commit();
                    return true;

                case R.id.navigation_guia:
                    Context context12 = getApplicationContext();
                    CharSequence text12 = "Guia fotografica de las plagas";
                    int duration12 = Toast.LENGTH_SHORT;
                    Toast toast12 = Toast.makeText(context12, text12, duration12);
                    toast12.show();
                    GuiaFragment guiaFragment = new GuiaFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,guiaFragment)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .addToBackStack(null).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        /*BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);*/
    }
}
